﻿namespace lesson7_shlomi
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.forfeit = new System.Windows.Forms.Button();
            this.lvlYou = new System.Windows.Forms.Label();
            this.lblOp = new System.Windows.Forms.Label();
            this.uscore = new System.Windows.Forms.Label();
            this.opscore = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // forfeit
            // 
            this.forfeit.Location = new System.Drawing.Point(387, 42);
            this.forfeit.Name = "forfeit";
            this.forfeit.Size = new System.Drawing.Size(75, 23);
            this.forfeit.TabIndex = 0;
            this.forfeit.Text = "Forfeit";
            this.forfeit.UseVisualStyleBackColor = true;
            this.forfeit.Click += new System.EventHandler(this.button1_Click);
            // 
            // lvlYou
            // 
            this.lvlYou.AutoSize = true;
            this.lvlYou.Location = new System.Drawing.Point(62, 47);
            this.lvlYou.Name = "lvlYou";
            this.lvlYou.Size = new System.Drawing.Size(61, 13);
            this.lvlYou.TabIndex = 1;
            this.lvlYou.Text = "Your score:";
            // 
            // lblOp
            // 
            this.lblOp.AutoSize = true;
            this.lblOp.Location = new System.Drawing.Point(667, 47);
            this.lblOp.Name = "lblOp";
            this.lblOp.Size = new System.Drawing.Size(86, 13);
            this.lblOp.TabIndex = 2;
            this.lblOp.Text = "Opponent score:";
            // 
            // uscore
            // 
            this.uscore.AutoSize = true;
            this.uscore.Location = new System.Drawing.Point(119, 47);
            this.uscore.Name = "uscore";
            this.uscore.Size = new System.Drawing.Size(13, 13);
            this.uscore.TabIndex = 3;
            this.uscore.Text = "0";
            // 
            // opscore
            // 
            this.opscore.AutoSize = true;
            this.opscore.Location = new System.Drawing.Point(749, 47);
            this.opscore.Name = "opscore";
            this.opscore.Size = new System.Drawing.Size(13, 13);
            this.opscore.TabIndex = 4;
            this.opscore.Text = "0";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Lime;
            this.ClientSize = new System.Drawing.Size(1251, 445);
            this.Controls.Add(this.opscore);
            this.Controls.Add(this.uscore);
            this.Controls.Add(this.lblOp);
            this.Controls.Add(this.lvlYou);
            this.Controls.Add(this.forfeit);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button forfeit;
        private System.Windows.Forms.Label lvlYou;
        private System.Windows.Forms.Label lblOp;
        private System.Windows.Forms.Label uscore;
        private System.Windows.Forms.Label opscore;
    }
}

